
thank you: https://forum.arduino.cc/index.php?topic=393655.0
& https://github.com/RedBearLab/CCLoader for code

http://www.jnhuamao.cn/bluetooth.asp?ID=1 for firmware & docs
http://www.jnhuamao.cn/bluetooth40_en.zip hm-10 firmware download
CCLoader
========

Burn CC254x firmware using an Arduino board.

1. Load the CCLoader Arduino sketch to the UNO board.
2. Wire the pins:
  ![image](CCLoader.jpg)
3. Use CCLoader.exe to load the Demo.bin to the UNO board and the board will burn the firmware to the BLE Mini.
