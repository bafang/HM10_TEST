/*
AT: OK
get_bt_version: HMSoft V540
get_bt_identifier: OK+NAME:ArduinoHM10 
get_bt_pin: OK+Get:004545
get_bt_baud: OK+Get:0
get_bt_uart_sleep: 
get_bt_bond: OK+Get:0
get_bt_timeout: OK+Get:000000
get_bt_role: OK+Get:1
get_bt_powerm: OK+Get:1
get_bt_power: OK+Get:3
get_bt_notify: OK+Get:0
get_bt_mode: OK+Get:0

AT+SLUP firmware upgrade mode
 */
//iBeacon UUID is: 74278BDA-B644-4520-8F0C-720EAF059935
//bluetooth -----------------------------------------------------
int responseTime = 2850;

void print_response(){
  delay(responseTime);
  //Serial1.available();
  String response = Serial1.readString();
  Serial.print(response);
  Serial.print("\n");  
}
void start_bt_at(){
  Serial1.print("AT");
  Serial1.flush();

  Serial.print("AT: ");
  print_response();
}
void reset_bt(){
  //AT+RENEW
  //AT+RESET
 //9600N81, 000000
  Serial1.print("AT+HMSOFT");
  Serial1.flush();

  Serial.print("reset_bt: ");
  print_response();
}

void get_bt_mode(){
  Serial1.print("AT+MODE?");
  Serial1.flush();
  Serial.print("get_bt_mode: ");
  print_response();   
}
void set_bt_mode(){
  Serial1.print("AT+MODE0"); //0,1,2
  Serial1.flush();
  Serial.print("set_bt_mode: ");
  print_response();   
}
void get_bt_notify(){
  Serial1.print("AT+NOTI?"); //0 don't notify, 1 notify
  Serial1.flush();
  Serial.print("get_bt_notify: ");
  print_response(); 
}
void get_bt_power(){  
  Serial1.print("AT+POWE?"); //0 -23dbm ,1 -6dbm ,2 0dbm ,3 6dbm
  Serial1.flush();
  Serial.print("get_bt_power: ");
  print_response();   
}
void get_bt_powerm(){  
  Serial1.print("AT+PWRM?"); //0 autosleep ,1 no autosleep
  Serial1.flush();
  Serial.print("get_bt_powerm: ");
  print_response();  
}
void get_bt_role(){
  Serial1.print("AT+ROLE?"); //0 peripherial ,1 central DEFAULT
  Serial1.flush();
  Serial.print("get_bt_role: ");
  print_response();  
}
void get_bt_timeout(){
  //Para1 allowed value: 000000~999999 Unit is ms.  Default: 000000 Connect forever
  Serial1.print("AT+TCON?");
  Serial1.flush();
  Serial.print("get_bt_timeout: ");
  print_response();   
}
void get_bt_bond(){ //DEFAULT 0
  //0:Not need PIN Code 1:Bond not need PIN 2:Bond with PIN 
  Serial1.print("AT+TYPE?");
  Serial1.flush();
  Serial.print("get_bt_bond: ");
  print_response();   
}
void get_bt_uart_sleep(){ //NOT SUPPORTED?, NO RESPONSE
  //0 sleep mode on uart runing, 1 sleep mode on uart not running
  Serial1.print("AT+UART?");
  Serial1.flush();
  Serial.print("get_bt_uart_sleep: ");
  print_response();  
}

void get_bt_baud(){
  Serial1.print("AT+BAUD?");
  Serial1.flush();
  Serial.print("get_bt_baud: ");
  print_response(); 
}
void set_bt_baud(long baud){
  String at_change_baud = String("AT+BAUD");
  switch(baud){
    case 1200:
      at_change_baud.concat("7"); //WILL ACCEPT BUT KILLS AT INTREPRETER, HAVE TO REFLASH
      break;
    case 2400:
      at_change_baud.concat("6");
      break;
    case 4800:
      at_change_baud.concat("5");
      break;
    case 9600:
      at_change_baud.concat("0");
      break;
    case 19200:
      at_change_baud.concat("1");
      break;
    case 38400:
      at_change_baud.concat("2");
      break;
    case 57600:
      at_change_baud.concat("3");
      break;
    case 115200:
      at_change_baud.concat("4");
      break;
    case 230400:
      at_change_baud.concat("8");
      break;
  }
  Serial.print(at_change_baud);
  Serial1.print(at_change_baud);
  Serial1.flush();            //wait for bytes to be sent
  //Serial1.end();              //restart @ baud, HAVE TO POWER CYCLE HM10 FOR BAUD CHANGE
  //Serial1.begin(baud);
 
  Serial.print("set_bt_baud: ");      //PRINT RESPONSE BEFORE BAUD CHANGE
  print_response();
}

void get_bt_identifier(){
  Serial1.print("AT+NAME?");
  Serial1.flush();
  Serial.print("get_bt_identifier: ");
  print_response();
}

void set_bt_identifier(String identifier){
  String at_change_identifier = String("AT+NAME");
  at_change_identifier.concat(identifier);

  Serial1.print(at_change_identifier);
  print_response();
}

void get_bt_version(){    
  Serial1.print("AT+VERR?");
  Serial1.flush();
  Serial.print("get_bt_version: ");
  print_response();  
}

void get_bt_pin(){
  Serial1.print("AT+PASS?");
  Serial1.flush();
  Serial.print("get_bt_pin: ");
  print_response();
}
void set_bt_pin(String pin){
  String at_change_identifier = String("AT+PASS");
  at_change_identifier.concat(pin);

  Serial1.print(at_change_identifier);

  Serial.print("set_bt_pin: ");
  print_response();
}

boolean isBtConnected = false;
boolean lastState = false;
boolean testingConnection = false;
long timer = 0;
void get_bt_connected(int pin){
    lastState = digitalRead(pin);
  //state pin = 2, same as connect led
  if(lastState & !testingConnection){
    //Serial.print("true");
    //isConnected = true;
    timer = millis();
    testingConnection = true;
    //Serial.println("testingConnection: ");
  } else if(testingConnection & lastState & !isBtConnected){
    //Serial.print("false");
    //isConnected = false;
    long diff = millis() - timer;
    //Serial.println(diff);
    if(diff > 650){
      isBtConnected = true;
      //Serial.println("isConnected");
    }
  } else if(!lastState & isBtConnected){
    isBtConnected = false;
    testingConnection = false;
    //Serial.println("disConnected");
  } else if(!lastState & !isBtConnected){
    //
    testingConnection = false;
  }
}

long find_bt_baud(){
  long baud[] = {1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200};
  String response;
  for(int i=0;i< 8;i++){
    Serial.print("trying baud: ");
    Serial.print(baud[i]);
    Serial.print("\n");

    Serial1.begin(baud[i]);
    Serial1.setTimeout(5000);
    while(!Serial1){}
    
    Serial1.print("AT");
    Serial1.flush();
    response = Serial1.readString();
    Serial.print("AT: ");
    Serial.println(response);
    if(response.equals("OK")){ //reads OK for all commands????
      Serial.println("found baud");
      Serial1.print("AT+VERR?");
      Serial1.flush();

      response = Serial1.readString();
      Serial.println(response);  
      //Serial1.end();
      //return baud[i];
      //set_bt_baud(9600);
      return 0;
    }
    Serial1.end();
  }
  return 0;
}

void setup(){
  delay(1500);
  pinMode(2, INPUT);      //bt connected pin
  pinMode(4, INPUT);      //bt reset pin
  pinMode(5, INPUT);      //bt dc pin
  pinMode(6, INPUT);      //bt dd pin

  Serial.begin(115200);
  //have to initalize to default 9600
  //find_bt_baud();
  Serial1.begin(115200);
  Serial2.begin(1200);
  while(!Serial | !Serial1 | !Serial2){}
  //start_bt_at();
  //get_bt_version();  
  //Serial1.print("AT+RENEW");  print_response();
  //Serial1.print("AT+RESET");    print_response();
  //Serial1.print("AT+HMSOFT");
  //print_response();  
  //set_bt_baud(9600);
  //Serial1.end();
  //Serial1.begin(9600);

  /*get_bt_identifier();
  get_bt_pin();
  get_bt_baud();
  get_bt_uart_sleep();
  get_bt_bond();
  get_bt_timeout();
  get_bt_role();
  get_bt_powerm();
  get_bt_power();
  get_bt_notify();
  get_bt_mode();*/
  //reset_bt();  
  /*while (Serial1.available() > 0) {
    start_bt_at();
    reset_bt();
    //set_bt_baud(1200);
    get_bt_version();
    get_bt_identifier();
    get_bt_pin();
    get_bt_baud();
    //set_bt_identifier(identifier);
    //set_bt_pin("004545");
    //if(baud != 9600)
    //  set_bt_baud(baud);
    //serial_set_timeout(bt_port, 1000); //set time out for waiting for read data
  }*/
}


typedef enum {at_term, bt_forward} modes;
modes mode = bt_forward;

//“AT+EXIT”

void loop(){
  /*while (Serial1.available() > 0) {
    //Serial.print(i++);
  }*/
  /*get_bt_connected(2);
  if(isBtConnected){
    
  } else {
    
  }*/


  if(mode == at_term){
    if(Serial.available()){
      Serial1.write(Serial.read());
    }
    if(Serial1.available()){
      Serial.write(Serial1.read());
    }
  }

//forward bytes between bt & serial2, monitor on usb_serial
//controller connected to serial2 so rx/tx is for that
  else if(mode == bt_forward){
    if(Serial2.available()){
      byte b2 = Serial2.read(); //s2 read = rx
      Serial1.write(b2);
      Serial.print("rx: ");
      if(b2 < 16)
        Serial.print("0");      
      Serial.print(b2, HEX);
      Serial.print("\n");
    }
    if(Serial1.available()){
      byte b1 = Serial1.read();
      Serial2.write(b1);      //s2 write = tx
      Serial.print("tx: ");
      if(b1 < 16)
        Serial.print("0");
      Serial.print(b1, HEX);
      Serial.print("\n");      
    }
  }
}
